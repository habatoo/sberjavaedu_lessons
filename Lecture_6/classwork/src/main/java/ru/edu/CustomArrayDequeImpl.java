package ru.edu;

import java.util.NoSuchElementException;

public class CustomArrayDequeImpl<E> {

    private Object[] data;
    private int head;
    private int tail;
    private int size;

    public CustomArrayDequeImpl() {

        this.data = new Object[10 + 1];
        this.head = this.tail = 0;
        this.size = 0;
    }

    public void addLast(E item) {

        if (isFull()) {
            throw new IllegalStateException("Deque is full. Sorry");
        }

        data[tail] = item;

        size++;
        tail++;
        if (tail == data.length) {
            tail = 0;
        }
    }

    public E removeLast() {

        if (isEmpty()) {
            throw new NoSuchElementException("Deque is empty. Sorry");
        }


        if (tail == 0) {
            tail = data.length - 1;
        } else {
            tail = tail - 1;
        }
        --size;

        return (E) data[tail];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private boolean isFull() {

        if (head == 0 && tail == data.length - 1) {
            return true;
        }

        if (head == tail + 1) {
            return true;
        }

        return false;
    }

    public boolean offerLast(E item) {

        try {
            addLast(item);
            return true;
        } catch (IllegalStateException ignored) {
            return false;
        }
    }

    public int size() {
        return this.size;
    }

    @Override
    public String toString() {

        StringBuilder cb = new StringBuilder();
        cb.append("[ ");

        int tmp = head;

        while (tmp != tail) {

            cb.append(data[tmp]);
            cb.append(" ");

            tmp++;
            if (tmp == data.length) {
                tmp = 0;
            }
        }

        cb.append(" ]");
        return cb.toString();
    }
}

package ru.edu;

import java.util.NoSuchElementException;

public class CustomLinkedListImpl<E> {

    private int size;
    private Node<E> first;
    private Node<E> last;

    private static class Node<E> {

        private E item;
        private Node<E> prev;
        private Node<E> next;

        private Node(E item) {
            this.item = item;
            this.prev = this.next = null;
        }
    }

    public CustomLinkedListImpl() {

        this.first = new Node<>(null);
        this.last = this.first;
        this.size = 0;
    }

    public int size() {
        return this.size;
    }

    public boolean add(E item) {

        insertAfterNode(last, item);
        return true;
    }

    public E removeFirst() {

        if (size == 0) {
            throw new NoSuchElementException("List is empty");
        }

        Node<E> removedNode = removeNode(first.next);
        return removedNode.item;
    }

    private Node<E> removeNode(Node<E> node) {

        if (last == node) {
            last = last.prev;
            last.next = null;
        } else {
            if (node.next != null) {
                node.next.prev = node.prev;
            }
            node.prev.next = node.next;
        }

        --size;
        return node;
    }

    private Node<E> insertAfterNode(Node<E> node, E item) {

        Node<E> newNode = new Node<>(item);

        if (node.next != null) {
            node.next.prev = newNode;
            newNode.next = node.next;
        }

        newNode.prev = node;
        node.next = newNode;

        if (node == last) {
            last = newNode;
        }
        size++;

        return newNode;
    }

    public String toString() {

        StringBuilder cb = new StringBuilder();
        cb.append("[ ");

        Node<E> tmp = first.next;
        while (tmp != null) {
            cb.append(tmp.item);
            cb.append(" ");
            tmp = tmp.next;
        }

        cb.append("]");
        return cb.toString();
    }
}

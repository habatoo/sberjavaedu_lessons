package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomLinkedListTests {

    @Test
    public void linkedListTest() {

        CustomLinkedListImpl<Integer> list = new CustomLinkedListImpl<>();

        System.out.println(list);
        Assert.assertEquals(0, list.size());

        list.add(1);
        Assert.assertEquals(1, list.size());
        System.out.println(list);

        for (int i = 0; i < 100; ++i) {
            list.add(i);
        }

        Assert.assertEquals(101, list.size());
        System.out.println(list);
    }

    @Test
    public void removeElementTest() {

        CustomLinkedListImpl<Integer> list = new CustomLinkedListImpl<>();

        for (int i = 0; i < 100; ++i) {
            list.add(i);
        }

        Assert.assertEquals(100, list.size());
        System.out.println(list);

        System.out.println("Removed: " + list.removeFirst());
        System.out.println(list);

        while (list.size() != 0) {
            list.removeFirst();
        }
        System.out.println(list);
    }
}

package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomPriorityQueueImplTests {

    @Test
    public void priorityQueueImplTest() {

        CustomPriorityQueueImpl<String> queue = new CustomPriorityQueueImpl<>(String::compareTo);

        Assert.assertEquals(0, queue.getSize());
        System.out.println(queue);

        queue.add("100");
        queue.add("50");
        queue.add("1");

        Assert.assertEquals(3, queue.getSize());
        System.out.println(queue);

        queue.add("2");
        Assert.assertEquals(4, queue.getSize());
        System.out.println(queue);

        queue.add("0");
        Assert.assertEquals(5, queue.getSize());
        System.out.println(queue);

        while (queue.isEmpty()) {

            System.out.println("Elem: " + queue.poll());
        }
    }
}

#HW_L6_T3_CustomArrayDequeImpl

Реализовать класс CustomArrayDequeImpl<T>, который представляет двунаправленную очередь на основе цикличного массива.

Класс CustomArrayDequeImpl реализует интерфейс CustomArrayDeque<T>
Класс CustomArrayDequeImpl может хранить объекты любого типа
Класс CustomArrayDequeImpl имеет фиксированный размер. Динамическое расширение не предусмотрено

##Конструкторы

CustomArrayDequeImpl();
CustomArrayDequeImpl(int capacity);

Ссылка на CustomArrayDeque<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomArrayDeque
2. Добавить интерфейс CustomArrayDeque в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomArrayDequeImpl от ветки feature/CustomArrayDeque

4. Написать реализацию класса CustomArrayDequeImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomArrayDequeImpl в ветку feature/CustomArrayDeque

6. Каждый публичный метод класса CustomArrayDequeImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomArrayDeque<T> нельзя
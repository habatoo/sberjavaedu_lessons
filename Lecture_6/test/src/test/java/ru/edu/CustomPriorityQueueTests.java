package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CustomPriorityQueueTests {

    private static final Integer SIZE = 5;
    private CustomPriorityQueue<Integer> queue = null;//new CustomPriorityQueueImpl<>(SIZE, Integer::compareTo);

    @Test
    public void sizeTest() {

        Assert.assertEquals(0, queue.size());

        queue.add(1);
        Assert.assertEquals(1, queue.size());

        queue.remove();
        Assert.assertEquals(0, queue.size());
    }

    @Test
    public void isEmptyTest() {

        Assert.assertTrue(queue.isEmpty());

        queue.add(1);
        Assert.assertFalse(queue.isEmpty());

        queue.remove();
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void addTest() {

        for (int i = SIZE; i > 0; --i) {
            queue.add(i);
        }
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(queue.toArray()));
    }

    @Test
    public void offerTest() {

        for (int i = 1; i <= SIZE; ++i) {
            queue.offer(i);
        }
        Assert.assertFalse(queue.offer(100));
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(queue.toArray()));
    }

    @Test
    public void peekTest() {

        for (int i = SIZE; i > 0; --i) {
            Assert.assertTrue(queue.add(i));
        }

        for (int i = SIZE; i > 0; --i) {
            Assert.assertEquals(Integer.valueOf(i), queue.peek());
            Assert.assertEquals(Integer.valueOf(i), queue.poll());
        }

        Assert.assertNull(queue.peek());
    }

    @Test
    public void pollTest() {

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertTrue(queue.add(i));
        }

        for (int i = SIZE; i > 0; --i) {
            Assert.assertEquals(Integer.valueOf(i), queue.poll());
        }
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void removeTest() {

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertTrue(queue.add(i));
        }

        for (int i = SIZE; i > 0; --i) {
            Assert.assertEquals(Integer.valueOf(i), queue.remove());
        }
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void toStringTest() {

        for (int i = SIZE; i > 0; --i) {
            queue.add(i);
        }
        Assert.assertEquals("[ 1 2 3 4 5 ]", queue.toString());
    }

    @Test
    public void toArrayTest() {

        for (int i = SIZE; i > 0; --i) {
            queue.add(i);
        }
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(queue.toArray()));
    }
}

package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CustomDequeTests {

    private static final int SIZE = 6;
    private CustomArrayDeque deque = null;//new CustomArrayDequeImpl(SIZE);

    @Test
    public void sizeTest() {

        Assert.assertTrue(deque.isEmpty());

        for (int i = 1; i <= SIZE; ++i) {
            deque.add(i);
            Assert.assertEquals(i, deque.size());
        }

        Assert.assertFalse(deque.isEmpty());
    }

    @Test
    public void isEmptyTest() {

        Assert.assertTrue(deque.isEmpty());
        deque.add(1);
        Assert.assertFalse(deque.isEmpty());
        deque.removeFirst();
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void addTest() {

        for (int i = 1; i <= 5; ++i) {
            deque.add(i);
        }

        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(deque.toArray()));
    }

    @Test
    public void addFirstTest() {

        for (int i = 1; i <= 5; ++i) {
            deque.addFirst(i);
        }

        Assert.assertEquals(Arrays.asList(5, 4, 3, 2, 1), Arrays.asList(deque.toArray()));
    }

    @Test
    public void addLastTest() {

        for (int i = 1; i <= 5; ++i) {
            deque.addLast(i);
        }

        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(deque.toArray()));
    }

    @Test
    public void offerFirstTest() {

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertTrue(deque.offerFirst(i));
        }

        Assert.assertFalse(deque.offerFirst(100));
        Assert.assertEquals(Arrays.asList(6, 5, 4, 3, 2, 1), Arrays.asList(deque.toArray()));
    }

    @Test
    public void offerLastTest() {

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertTrue(deque.offerLast(i));
        }

        Assert.assertFalse(deque.offerLast(100));
        Assert.assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6), Arrays.asList(deque.toArray()));
    }

    @Test
    public void getFirstTest() {

        deque.addLast(10);
        Assert.assertEquals(10, deque.getFirst());

        deque.addFirst(100);
        Assert.assertEquals(100, deque.getFirst());

        deque.addFirst(1000);
        Assert.assertEquals(1000, deque.getFirst());
    }

    @Test
    public void getLastTest() {

        deque.addFirst(10);
        Assert.assertEquals(10, deque.getLast());

        deque.addLast(100);
        Assert.assertEquals(100, deque.getLast());

        deque.addLast(1000);
        Assert.assertEquals(1000, deque.getLast());
    }

    @Test
    public void peekFirstTest() {

        Assert.assertNull(deque.peekFirst());

        deque.addLast(10);
        Assert.assertEquals(10, deque.peekFirst());

        deque.addFirst(100);
        Assert.assertEquals(100, deque.peekFirst());

        deque.addFirst(1000);
        Assert.assertEquals(1000, deque.peekFirst());
    }

    @Test
    public void peekLastTest() {

        Assert.assertNull(deque.peekLast());

        deque.addFirst(10);
        Assert.assertEquals(10, deque.peekLast());

        deque.addLast(100);
        Assert.assertEquals(100, deque.peekLast());

        deque.addLast(1000);
        Assert.assertEquals(1000, deque.peekLast());
    }

    @Test
    public void pollFirstTest() {

        for (int i = 1; i <= SIZE; ++i) {
            deque.addLast(i);
        }

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertEquals(i, deque.pollFirst());
        }
        Assert.assertNull(deque.pollFirst());
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void pollLastTest() {

        for (int i = 1; i <= SIZE; ++i) {
            deque.addLast(i);
        }

        for (int i = SIZE; i > 0; --i) {
            Assert.assertEquals(i, deque.pollLast());
        }
        Assert.assertNull(deque.pollLast());
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void removeFirstTest() {

        for (int i = 1; i <= SIZE; ++i) {
            deque.addLast(i);
        }

        for (int i = 1; i <= SIZE; ++i) {
            Assert.assertEquals(i, deque.removeFirst());
        }
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void removeLastTest() {
        for (int i = 1; i <= SIZE; ++i) {
            deque.addLast(i);
        }

        for (int i = SIZE; i > 0; --i) {
            Assert.assertEquals(i, deque.removeLast());
        }
        Assert.assertTrue(deque.isEmpty());
    }

    @Test
    public void toStringTest() {

        deque.addFirst(1);
        deque.addFirst(2);
        deque.addLast(3);
        deque.addLast(4);

        Assert.assertEquals("[ 2 1 3 4 ]", deque.toString());
    }


    @Test
    public void toArrayTest() {

        deque.addFirst(1);
        deque.addFirst(2);
        deque.addLast(3);
        deque.addLast(4);

        Assert.assertEquals(2, deque.getFirst());
        Assert.assertEquals(2, deque.peekFirst());

        Assert.assertEquals(4, deque.getLast());
        Assert.assertEquals(4, deque.peekLast());

        Assert.assertEquals(Arrays.asList(2, 1, 3, 4), Arrays.asList(deque.toArray()));
    }
}

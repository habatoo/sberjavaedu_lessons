package ru.edu;

public class Car implements Comparable<Car> {

    private String model;
    private String color;
    private int year;

    public Car(String model, String color, int year) {
        this.model = model;
        this.color = color;
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }

        if (other == this) {
            return true;
        }

        if (!this.getClass().equals(other.getClass())) {
            return false;
        }

        Car otherCar = (Car) other;

        return this.model.equals(otherCar.model) &&
                this.color.equals(otherCar.color);
    }

    @Override
    public int hashCode() {
        return this.model.length();
    }

    @Override
    public int compareTo(Car other) {

        if (this.year < other.year) {
            return -1;
        }
        if (this.year > other.year) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {

        return "Car{" +
                "model=" + model +
                " color=" + color +
                " year=" + year +
                "}";
    }
}

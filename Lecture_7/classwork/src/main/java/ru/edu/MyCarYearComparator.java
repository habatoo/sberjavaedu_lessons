package ru.edu;

import java.util.Comparator;

public class MyCarYearComparator implements Comparator<Car> {

    @Override
    public int compare(Car lhs, Car rhs) {

        return lhs.getYear() - rhs.getYear();
    }
}

package ru.edu;

import java.util.Comparator;

public class CustomTreeSetImpl<E> {

    public CustomTreeSetImpl(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    private Comparator<E> comparator;
    private Node<E> root;

    public int getSize() {
        return size;
    }

    private int size;

    private class Node<E> {

        private E item;
        private Node<E> left;
        private Node<E> right;
        private Node<E> parent;

        public Node(E item) {
            this.item = item;
        }

        public void setLeftChild(Node<E> node) {

            this.left = node;

            if (this.left != null) {
                this.left.parent = this;
            }
        }

        public void setRightChild(Node<E> node) {

            this.right = node;

            if (this.right != null) {
                this.right.parent = this;
            }
        }
    }

    public boolean add(E item) {

        if (root == null) {
            root = new Node<>(item);
            size = 1;
            return true;
        }

        Node<E> insertedNode = insertNode(root, item);
        return insertedNode != null;
    }

    private Node<E> insertNode(Node<E> node, E item) {

        int result = comparator.compare(item, node.item);
        if (result < 0) {
            if (node.left == null) {
                node.setLeftChild(new Node<>(item));
                size++;
                return node.left;
            } else {
                insertNode(node.left, item);
            }
        }

        if (result > 0) {
            if (node.right == null) {
                node.setRightChild(new Node<>(item));
                size++;
                return node.right;
            } else {
                insertNode(node.right, item);
            }
        }

        return null;
    }

    public boolean contains(E item) {

        if (root == null) {
            return false;
        }

        Node<E> foundItem = findNodeByItem(root, item);
        return foundItem != null;
    }

    private Node<E> findNodeByItem(Node<E> node, E item) {

        int result = comparator.compare(item, node.item);

        if (result == 0) {
            return node;
        }

        Node<E> searchIn = result < 0 ? node.left : node.right;
        if (searchIn == null) {
            return null;
        }
        return findNodeByItem(searchIn, item);
    }

    @Override
    public String toString() {

        StringBuilder cb = new StringBuilder();

        cb.append("[");

        if (root != null) {
            printTree(cb, root);
        }

        cb.append(" ]");
        return cb.toString();
    }

    private void printTree(StringBuilder cb, Node<E> node) {

        if (node.left != null) {
            printTree(cb, node.left);
        }
        cb.append(" " + node.item);
        if (node.right != null) {
            printTree(cb, node.right);
        }
    }
}

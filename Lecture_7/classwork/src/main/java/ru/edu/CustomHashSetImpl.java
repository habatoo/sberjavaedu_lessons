package ru.edu;

import java.util.Objects;

public class CustomHashSetImpl<E> {

    private int size;
    private Node<E>[] table;

    public int getSize() {
        return size;
    }

    private static class Node<E> {

        private E item;
        private int hash;
        private Node<E> next;

        public Node(E item, int hash) {
            this.item = item;
            this.hash = hash;
        }
    }

    public CustomHashSetImpl() {

        this.table = new Node[16 + 1];
        for (int i = 0; i < table.length; ++i) {
            table[i] = new Node<>(null, Integer.MAX_VALUE);
        }

        this.size = 0;
    }

    public boolean add(E item) {

        int itemHash = getItemHash(item);
        int bucketId = getBucketId(item);

        Node<E> node = table[bucketId];
        while (node.next != null) {

            Node<E> nodeForAnalyse = node.next;
            if (nodeForAnalyse.hash == itemHash && Objects.equals(nodeForAnalyse.item, item)) {
                return false;
            }
            node = node.next;
        }

        node.next = new Node<>(item, itemHash);
        ++size;
        return true;
    }

    public boolean contains(E item) {

        int itemHash = getItemHash(item);
        int bucketId = getBucketId(item);

        Node<E> node = table[bucketId];
        while (node.next != null) {

            Node<E> nodeForAnalyse = node.next;
            if (nodeForAnalyse.hash == itemHash && Objects.equals(nodeForAnalyse.item, item)) {
                return true;
            }
            node = node.next;
        }

        return false;
    }

    private int getBucketId(E item) {

        if (item == null) {
            return 0;
        }

        return getItemHash(item) % (table.length - 1) + 1;
    }

    private int getItemHash(E item) {

        return Objects.nonNull(item) ? Math.abs(item.hashCode()) : 0;
    }

    @Override
    public String toString() {

        StringBuilder cb = new StringBuilder();

        cb.append("[");

        for (int i = 0; i < table.length; ++i) {

            Node<E> tmp = table[i].next;
            while (tmp != null) {
                cb.append(" " + tmp.item);
                tmp = tmp.next;
            }
        }

        cb.append(" ]");

        return cb.toString();
    }
}

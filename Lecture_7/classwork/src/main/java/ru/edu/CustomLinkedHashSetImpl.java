package ru.edu;

import java.util.Objects;

public class CustomLinkedHashSetImpl<E> {

    private int size;
    private Node<E>[] table;
    private Node<E> head;
    private Node<E> tail;

    public int getSize() {
        return size;
    }

    private static class Node<E> {

        private E item;
        private int hash;
        private Node<E> next;
        private Node<E> prev;
        private Node<E> after;

        public Node(E item, int hash) {
            this.item = item;
            this.hash = hash;
        }
    }

    public CustomLinkedHashSetImpl() {

        this.table = new Node[16 + 1];
        for (int i = 0; i < table.length; ++i) {
            table[i] = new Node<>(null, Integer.MAX_VALUE);
        }

        this.head = this.tail = new Node<>(null, Integer.MAX_VALUE);
        this.size = 0;
    }

    public boolean add(E item) {

        int itemHash = getItemHash(item);
        int bucketId = getBucketId(item);

        Node<E> node = table[bucketId];
        while (node.next != null) {

            Node<E> nodeForAnalyse = node.next;
            if (nodeForAnalyse.hash == itemHash && Objects.equals(nodeForAnalyse.item, item)) {
                return false;
            }
            node = node.next;
        }

        Node<E> newNode = new Node<>(item, itemHash);

        newNode.prev = tail;
        tail.after = newNode;
        tail = newNode;

        node.next = newNode;
        ++size;
        return true;
    }

    public boolean contains(E item) {

        int itemHash = getItemHash(item);
        int bucketId = getBucketId(item);

        Node<E> node = table[bucketId];
        while (node.next != null) {

            Node<E> nodeForAnalyse = node.next;
            if (nodeForAnalyse.hash == itemHash && Objects.equals(nodeForAnalyse.item, item)) {
                return true;
            }
            node = node.next;
        }

        return false;
    }

    private int getBucketId(E item) {

        if (item == null) {
            return 0;
        }

        return getItemHash(item) % (table.length - 1) + 1;
    }

    private int getItemHash(E item) {

        return Objects.nonNull(item) ? Math.abs(item.hashCode()) : 0;
    }

    @Override
    public String toString() {

        StringBuilder cb = new StringBuilder();

        cb.append("[");

        Node<E> tmp = head.after;
        while (tmp != null) {
            cb.append(" " + tmp.item);
            tmp = tmp.after;
        }

        cb.append(" ]");

        return cb.toString();
    }
}

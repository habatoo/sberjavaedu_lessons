package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomLinkedHashSetImplTests {

    @Test
    public void hashSetTest() {

        CustomLinkedHashSetImpl<String> set = new CustomLinkedHashSetImpl<>();

        Assert.assertEquals(0, set.getSize());

        set.add("Anton");
        set.add("Anton");
        set.add("Denis");
        set.add("Vasilisa");
        set.add("Nick");

        Assert.assertEquals(4, set.getSize());
        System.out.println(set);
    }
}

package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class PersonEqualityTests {

    @Test
    public void equalsTest() {

        Person lhs = new Person("Anton", "Petrov", 20);
        Person rhs = new Person("Anton", "Petrov", 20);

        Assert.assertEquals(lhs, rhs);
        Assert.assertEquals(lhs.hashCode(), rhs.hashCode());

        rhs = new Person("aNton", "petRov", 20);
        Assert.assertEquals(lhs, rhs);
        Assert.assertEquals(lhs.hashCode(), rhs.hashCode());

        rhs = new Person("Anton", "Petrov", 21);
        Assert.assertNotEquals(lhs, rhs);

        rhs = new Person("Anton", "Sidorov", 20);
        Assert.assertNotEquals(lhs, rhs);

        rhs = new Person("Denis", "Petrov", 20);
        Assert.assertNotEquals(lhs, rhs);
    }

    @Test
    public void differentTypesTest() {

        Person person = new Person("Anton", "Sidorov", 20);

        Assert.assertNotEquals(person, null);
        Assert.assertNotEquals(person, new Integer(10));
        Assert.assertNotEquals(person, new RuntimeException());
    }
}

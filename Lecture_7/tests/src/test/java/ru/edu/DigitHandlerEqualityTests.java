package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class DigitHandlerEqualityTests {

    @Test
    public void equalsTest() {

        DigitHandlerEquality lhs;
        DigitHandlerEquality rhs;

        for (int i = 0; i < 1000; ++i) {
            lhs = new DigitHandlerEquality(i);
            rhs = new DigitHandlerEquality(i);

            Assert.assertEquals(lhs, rhs);
            Assert.assertEquals(lhs.hashCode(), rhs.hashCode());

            lhs = new DigitHandlerEquality(i + 5);
            rhs = new DigitHandlerEquality(i - 5);
            Assert.assertNotEquals(lhs, rhs);
        }

        lhs = new DigitHandlerEquality(25);
        rhs = new DigitHandlerEquality(50);
        Assert.assertNotEquals(lhs, rhs);
    }

    @Test
    public void differentTypesTest() {

        Assert.assertNotEquals(new DigitHandlerEquality(10), null);
        Assert.assertNotEquals(new DigitHandlerEquality(10), new Integer(10));
        Assert.assertNotEquals(new DigitHandlerEquality(10), new RuntimeException());
    }
}

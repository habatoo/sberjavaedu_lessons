#HW_L7_T5_CustomLinkedHashSetImpl

Реализовать класс CustomLinkedHashSetImpl<T>, который представляет множество на хеш-таблицы.

Класс CustomLinkedHashSetImpl реализует интерфейс CustomLinkedHashSet<T>
Класс CustomLinkedHashSetImpl может хранить объекты любого типа
Методы toString & toArray должны возвращать элементы в том порядке, в котором элементы были добавлены в множество

##Конструкторы

CustomLinkedHashSetImpl();

Ссылка на CustomLinkedHashSet<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomLinkedHashSet
2. Добавить интерфейс CustomLinkedHashSet в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomLinkedHashSetImpl от ветки feature/CustomLinkedHashSet

4. Написать реализацию класса CustomLinkedHashSetImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomLinkedHashSetImpl в ветку feature/CustomLinkedHashSet

6. Каждый публичный метод класса CustomLinkedHashSetImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomLinkedHashSet<T> нельзя


#HW_L7_T4_CustomHashSetImpl

Реализовать класс CustomHashSetImpl<T>, который представляет множество на основе хеш-таблицы.

Класс CustomHashSetImpl реализует интерфейс CustomHashSet<T>
Класс CustomHashSetImpl может хранить объекты любого типа

##Конструкторы

CustomHashSetImpl();

Ссылка на CustomHashSet<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomHashSet
2. Добавить интерфейс CustomHashSet в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomHashSetImpl от ветки feature/CustomHashSet

4. Написать реализацию класса CustomHashSetImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomHashSetImpl в ветку feature/CustomHashSet

6. Каждый публичный метод класса CustomHashSetImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomHashSet<T> нельзя


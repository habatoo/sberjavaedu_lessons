package org.example;

import java.sql.*;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class PersonRepository {

    private final Connection connection;

    public PersonRepository(Connection connection) {
        this.connection = connection;
    }


    public void createTable() throws SQLException {

        Statement statement = connection.createStatement();

        String query = "create table person (" +
                "" +
                "id integer," +
                "first_name varchar(64)," +
                "second_name varchar(64)," +
                "last_name varchar(64)," +
                "birth_date TEXT" +
                ");";

        statement.executeUpdate(query);
    }

    public void deleteTable() throws SQLException {

        Statement statement = connection.createStatement();

        statement.executeUpdate("drop table person;");
    }

    public void addPerson(Person person) throws SQLException {

        Statement statement = connection.createStatement();

        String query = String.format("INSERT INTO person VALUES ('%s', '%s', '%s', '%s', '%s');",
                person.getId(),
                person.getFirstName(),
                person.getSecondName(),
                person.getLastName(),
                Timestamp.from(person.getBirthDate().toInstant()));

        int affectedRows = statement.executeUpdate(query);
        System.out.println("AffectedRows=" + affectedRows);
    }

    public void updatePersonLastName(String id, String lastName) throws SQLException {

        Statement statement = connection.createStatement();
        int affectedRows = statement.executeUpdate("UPDATE person set last_name='" + lastName + "' WHERE id='" + id + "';");
        System.out.println("AffectedRows=" + affectedRows);
    }

    public void deleteById(int id) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("DELETE FROM person2 WHERE id = ?;");

        statement.setInt(1, id);

        int affectedRows = statement.executeUpdate();
        System.out.println("AffectedRows=" + affectedRows);
    }

    public List<Person> getAllPersons() throws SQLException {

        Statement statement = connection.createStatement();
        String query = "SELECT id, last_name, first_name, birth_date from person LIMIT 5;";

        ResultSet resultSet = statement.executeQuery(query);
        System.out.println(resultSet);

        List<Person> personList = new ArrayList<>();

        while (resultSet.next()) {

            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Timestamp sqlTimestamp = resultSet.getTimestamp("birth_date");

            Person person = new Person(id, firstName, "", lastName, OffsetDateTime.ofInstant(sqlTimestamp.toInstant(), ZoneId.systemDefault()));
            personList.add(person);
        }

        return personList;
    }
}

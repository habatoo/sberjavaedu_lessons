package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Hello world!
 */
public class App {
    private static final String JDBC_URL = "jdbc:sqlite:D:\\work\\SberJavaEdu2021_Lessons\\Lecture_12\\classwork\\empty_db\\chinook.db";

//    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/testdb?user=postgres&password=postgres";

    public static void main(String[] args) throws SQLException {

        Connection connection = DriverManager.getConnection(JDBC_URL);

        PersonRepository repository = new PersonRepository(connection);

        repository.createTable();
//        repository.deleteTable();
//        Person person = new Person(UUID.randomUUID(), "Denis", "Ivanovich", "Ivanov", LocalDateTime.of(1900, 1, 1, 10, 30));
//        repository.addPerson(person);
//
        for (int i = 1; i <= 10; ++i) {
            Person person = new Person(i, "Denis" + i, "", "Ivanov" + i, OffsetDateTime.now());
            repository.addPerson(person);
        }
//
//        repository.updatePersonLastName("d3587f1e-0315-4c04-860b-bb8c6b4cf944", "Pertov");
//
        List<Person> personList = repository.getAllPersons();
        for (Person person : personList) {
            System.out.println(person);
        }
    }
}
